//
//  ViewController.swift
//  SwiftImageEdit
//
//  Created by Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    var origImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photo Filters"
    }

    @IBAction func choosePhoto(_ sender: Any) {
        openPhotoLibrary()
    }
    
    @IBAction func makeNegative(_ sender: Any) {
        guard let image = origImage else {return}
        imageView.image = OpenCVWrapper.toNeg(image)
    }
    
    @IBAction func makeGray(_ sender: Any) {
        guard let image = origImage else {return}
        imageView.image = OpenCVWrapper.toGray(image)
    }
    
    @IBAction func filter1(_ sender: Any) {
        guard let image = origImage else {return}
        imageView.image = OpenCVWrapper.toFil1(image)
    }
    
    @IBAction func filter2(_ sender: Any) {
        guard let image = origImage else {return}
        imageView.image = OpenCVWrapper.toFil2(image)
    }
    
    
    @IBAction func removeFilter(_ sender: Any) {
        guard let image = origImage else {return}
        imageView.image = image
    }
}

private typealias SelectPicture = ViewController
extension SelectPicture: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openPhotoLibrary() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            print("Can't access photos")
            return
        }
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {return}
        self.origImage = image
        imageView.image = image
    }
}
