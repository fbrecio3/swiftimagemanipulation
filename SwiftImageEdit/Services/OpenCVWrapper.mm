//
//  OpenCVWrapper.mm
//  SwiftImageEdit
//
//  Created by Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

//Prevents warnings caused by OpenCV Documentation
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#pragma clang pop

//Allows code to be written without using std:: or cv::
using namespace std;
using namespace cv;

//Private declarations
@interface OpenCVWrapper ()
+ (Mat)_negFrom:(Mat)source;
+ (Mat)_grayFrom:(Mat)source;
+ (Mat)_matFrom:(UIImage *)source;
+ (UIImage *)_imageFrom:(Mat)source;

@end


@implementation OpenCVWrapper

//MARK: Public functions visible in Swift
+ (UIImage *)toNeg:(UIImage *)source {
    return [OpenCVWrapper _imageFrom:[OpenCVWrapper _negFrom:[OpenCVWrapper _matFrom:source]]];
}

+ (UIImage *)toGray:(UIImage *)source {
    return [OpenCVWrapper _imageFrom:[OpenCVWrapper _grayFrom:[OpenCVWrapper _matFrom:source]]];
}

+ (UIImage *)toFil1:(UIImage *)source {
    return [OpenCVWrapper _imageFrom:[OpenCVWrapper _fil1From:[OpenCVWrapper _matFrom:source]]];
}

+ (UIImage *)toFil2:(UIImage *)source {
    return [OpenCVWrapper _imageFrom:[OpenCVWrapper _fil2From:[OpenCVWrapper _matFrom:source]]];
}

//MARK: Private functions
//OpenCV Image Manipulation functions
+ (Mat)_negFrom:(Mat)source {
    Mat result;
    
    cvtColor(source, result, CV_BGR2HSV);
    
    return result;
}

+ (Mat)_grayFrom:(Mat)source {
    Mat result;
    
    cvtColor(source, result, CV_BGR2GRAY);
    
    return result;
}

+ (Mat)_fil1From:(Mat)source {
    Mat result;
    
    cvtColor(source, result, CV_BGR2YUV);
    
    return result;
}

+ (Mat)_fil2From:(Mat)source {
    Mat result;
    
    cvtColor(source, result, CV_BGR2HLS);
    
    return result;
}


//Image type conversion functions
+ (Mat)_matFrom:(UIImage *)source {
    CGImageRef image = CGImageCreateCopy(source.CGImage);
    CGFloat cols = CGImageGetWidth(image);
    CGFloat rows = CGImageGetHeight(image);
    Mat result(rows, cols, CV_8UC4);
    
    CGBitmapInfo bitmapFlags = kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault;
    size_t bitsPerComponent = 8;
    size_t bytesPerRow = result.step[0];
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image);
    
    CGContextRef context = CGBitmapContextCreate(result.data, cols, rows, bitsPerComponent, bytesPerRow, colorSpace, bitmapFlags);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, cols, rows), image);
    CGContextRelease(context);
    
    return result;
}

+ (UIImage *)_imageFrom:(Mat)source {
    NSData *data = [NSData dataWithBytes:source.data length:source.elemSize() * source.total()];
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    CGBitmapInfo bitmapFlags = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    size_t bitsPerComponent = 8;
    size_t bytesPerRow = source.step[0];
    CGColorSpaceRef colorSpace = (source.elemSize() == 1 ? CGColorSpaceCreateDeviceGray() : CGColorSpaceCreateDeviceRGB());
    
    CGImageRef image = CGImageCreate(source.cols, source.rows, bitsPerComponent, bitsPerComponent * source.elemSize(), bytesPerRow, colorSpace, bitmapFlags, provider, NULL, false, kCGRenderingIntentDefault);
    UIImage *result = [UIImage imageWithCGImage:image];
    
    CGImageRelease(image);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return result;
}

@end
