//
//  OpenCVWrapper.h
//  SwiftImageEdit
//
//  Created by Admin on 3/13/18.
//  Copyright © 2018 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

+ (UIImage *)toNeg:(UIImage *)source;
+ (UIImage *)toGray:(UIImage *)source;
+ (UIImage *)toFil1:(UIImage *)source;
+ (UIImage *)toFil2:(UIImage *)source;

@end
